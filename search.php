<?php
/**
 * The template for displaying search results pages.
 *
 * @package basic
 */

get_header(); 
?>
<div class="clearfix"></div>
<main id="content">
    <div class="row">
      <div class="<?php echo ( !is_active_sidebar( 'sidebar-1' ) ? 'col-md-12 col-lg-12' :'col-md-9 col-lg-9 ' ); ?>">
		         <?php 
				global $i;
				if ( have_posts() ) : ?>
				<h2 class="search-result"><?php printf( esc_html__( "Search Results for: %s", 'basic' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
				<?php while ( have_posts() ) : the_post();  
				 get_template_part('content','');
				 endwhile; else : ?>
				<h2><?php esc_html_e('Not Found','basic'); ?></h2>
				<div class="">
				<p><?php esc_html_e('Sorry, Do Not match.','basic' ); ?>
				</p>
				<?php get_search_form(); ?>
				</div><!-- .blog_con_mn -->
				<?php endif; ?>
      </div>
	  <aside class="col-md-3 col-lg-3">
        <?php get_sidebar(); ?>
      </aside>
    </div>
</main>
<?php
get_footer();
?>