<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @package basic
 */
get_header(); ?>
<main id="content">
  	<div class="row">
		<!-- Blog Area -->
		<div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-md-12 col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">			<?php if( have_posts()) :  the_post(); ?>		
				<div class="pagemaincontent">
					<h1><?php the_title();?></h1>
					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '<div class="link page-break-links">' . __( 'Pages:', 'basic' ), 'after' => '</div>' ) ); ?>
				</div>
			<?php endif; ?>
			<?php if(get_comments_number() > 0): ?>
				<?php comments_template( '', true ); // show comments ?>
			<?php endif; ?>
		<!-- /Blog Area -->			
		</div>
		<!--Sidebar Area-->
		<aside class="col-md-3">
			<?php get_sidebar(); ?>
		</aside>
		<!--Sidebar Area-->
	</div>
</main>
<?php
get_footer();