<?php
/**
 * The template for displaying archive pages.
 *
 * @package basic
 */

get_header(); ?>

<main id="content">
    <div class="row">
      <div class="<?php echo ( !is_active_sidebar( 'sidebar-1' ) ? 'col-md-12 col-lg-12' :'col-md-9 col-lg-9 ' ); ?>col-sm-8">
      	<h1 class="archive_title">
      		<?php the_archive_title(); ?>
      	</h1>
			<?php 
			if( have_posts() ) :
			while( have_posts() ): the_post();
				get_template_part('content',''); 
			endwhile; endif;
			?>
	        <div class="pagination-margin text-center">
	          <?php
				//Previous / next page navigation
				the_posts_pagination( array(
				'prev_text'          => '<i class="fa fa-long-arrow-left"></i>',
				'next_text'          => '<i class="fa fa-long-arrow-right"></i>',
				'screen_reader_text' => ' ',
				) );
				?>
	        </div>
      </div>
	  <aside class="col-md-3 col-sm-4">
        <?php get_sidebar(); ?>
      </aside>
    </div>
</main>
<?php get_footer(); ?>