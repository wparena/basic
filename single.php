<!-- =========================
     Page Breadcrumb   
============================== -->
<?php get_header(); ?>
<div class="clearfix"></div>
<!-- =========================
     Page Content Section      
============================== -->
 <main id="content">
    <div class="row"> 
      <!--/ Main blog column end -->
      <div class="<?php echo ( !is_active_sidebar( 'sidebar-1' ) ? 'col-md-12 col-lg-12' :'col-md-9 col-lg-9 ' ); ?>col-sm-8">
		      <?php if(have_posts())
		        {
		      while(have_posts()) { the_post(); ?>
            <div class="basic-blog-post-box"> <a class="basic-blog-thumb">
			        <?php if(has_post_thumbnail()): ?>
			         <?php $defalt_arg =array('class' => "img-responsive"); ?>
			         <?php the_post_thumbnail('', $defalt_arg); ?>
			        <?php endif; ?></a>

              <?php
                if (('post' === get_post_type() && !post_password_required()) && (comments_open() || get_comments_number())) :
                  if ((has_post_thumbnail()) || (!has_post_thumbnail() && is_single())) :
                    ?>
                    <div class="comments-count">
                      <a href="<?php esc_url(comments_link()); ?>">
                        <?php
                        printf(_nx('<i class="fa fa-comment"></i> 1', '<i class="fa fa-comment"></i> %1$s', get_comments_number(), 'comments title', 'basic'), number_format_i18n(get_comments_number()));
                        ?>
                      </a>
                    </div>
                    <?php
                  endif;
                endif;
              ?>

              <article class="small">

                <h1><a><?php the_title(); ?></a></h1>

                <div class="basic-blog-category post-meta-data">
                  Posted By:<a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ));?>">
                  <?php the_author(); ?>
                  </a> 
                  In<a href="#">
                    <?php   $cat_list = get_the_category_list();
                    if(!empty($cat_list)) { ?>
                    <?php the_category(', '); ?>
                  </a>
                  <?php } ?>
                  on  <span><?php echo get_the_date( get_option( 'date_format' ) ); ?></span>
                </div>

                <?php the_content(); ?>
              </article>
              <?php wp_link_pages( array( 'before' => '<div class="link single-page-break">' . __( 'Pages:', 'basic' ), 'after' => '</div>' ) ); ?>
              <div class="category-tag-div">
                <?php the_tags( '<i class="fa fa-tag" aria-hidden="true"></i> ', ', ', '<br />' ); ?>
              </div>
            </div>
		      <?php } ?>
            <div class="media basic-info-author-block"> <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ));?>" class="basic-author-pic"> <?php echo get_avatar( get_the_author_meta( 'ID') , 150); ?> </a>
              <div class="media-body">
                About: <h4 class="media-heading"><?php the_author_posts_link(); ?></h4>
                <p><?php the_author_meta( 'description' ); ?></p>
                <div class="row">
                  <div class="col-md-6 col-pad7">
                    <ul class="list-inline info-author-social">
          					<?php 
          					$facebook_profile = get_the_author_meta( 'facebook_profile' );
          					if ( $facebook_profile && $facebook_profile != '' ) {
          					echo '<li class="facebook"><a href="' . esc_url($facebook_profile) . '"><i class="fa fa-facebook-square"></i></a></li>';
          					} 
					
          					$twitter_profile = get_the_author_meta( 'twitter_profile' );
          					if ( $twitter_profile && $twitter_profile != '' ) 
          					{
          					echo '<li class="twitter"><a href="' . esc_url($twitter_profile) . '"><i class="fa fa-twitter-square"></i></a></li>';
          					}
					
          					$google_profile = get_the_author_meta( 'google_profile' );
          					if ( $google_profile && $google_profile != '' ) {
          					echo '<li class="googleplus"><a href="' . esc_url($google_profile) . '" rel="author"><i class="fa fa-google-plus-square"></i></a></li>';
          					}
          					$linkedin_profile = get_the_author_meta( 'linkedin_profile' );
          					if ( $linkedin_profile && $linkedin_profile != '' ) {
          					   echo '<li class="linkedin"><a href="' . esc_url($linkedin_profile) . '"><i class="fa fa-linkedin-square"></i></a></li>';
          					}
          					?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
		      <?php } ?>
          <?php if ( comments_open() ) : ?>
            <?php comments_template('',true); ?>
          <?php endif; ?>
      </div>
      <div class="col-md-3 col-sm-4">
      <?php get_sidebar(); ?>
      </div>
    </div>
    <!--/ Row end -->
</main>
<?php get_footer(); ?>