<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package basic
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div class="wrapper">
    <div class="container">
      <header>
        <div class="row">
          <div class="col-lg-12">
              <div class="clearfix"></div>
              <div class="basic-main-nav">
                <div class="row">
                  <div class="col-md-5 col-lg-5">
                    <div class="navbar-header">
                    <!-- Logo -->
                    <?php
                    if(has_custom_logo())
                    {
                    // Display the Custom Logo
                    the_custom_logo();
                    }
                     else { ?>
                    <a class="navbar-brand" href="<?php echo esc_url(home_url( '/' )); ?>"><span class="site-title"><?php bloginfo('name'); ?></span>
        			      <br>
                    <span class="site-description"><?php echo esc_html(get_bloginfo( 'description', 'display' )); ?></span>   
                    </a>      
                    <?php } ?>
                    <!-- Logo -->
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <nav class="navbar navbar-default navbar-static-top navbar-wp">
                      <!-- navbar-toggle -->
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-wp"> <span class="sr-only"><?php __('Toggle Navigation','basic'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                      <!-- /navbar-toggle --> 
                      <!-- Navigation -->
                      
                      <div class="collapse navbar-collapse" id="navbar-wp">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav', 'fallback_cb' => 'basic_custom_navwalker::fallback' , 'walker' => new basic_custom_navwalker() ) ); ?>

                      </div>
                      <!-- /Navigation -->
                    </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </header>
    <!-- #masthead --> 