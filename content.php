<?php 
/**
 * The template for displaying the content.
 * @package basic
 */
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="basic-blog-post-box">
				<?php
					if (('post' === get_post_type() && !post_password_required()) && (comments_open() || get_comments_number())) :
						if ((has_post_thumbnail()) || (!has_post_thumbnail() && !is_single())) :
							?>
							<div class="comments-count">
								<a href="<?php esc_url(comments_link()); ?>">
									<?php
									printf(_nx('<i class="fa fa-comment"></i> 1', '<i class="fa fa-comment"></i> %1$s', get_comments_number(), 'comments title', 'basic'), number_format_i18n(get_comments_number()));
									?>
								</a>
							</div>
							<?php
						endif;
					endif;
				?>
			<article class="small">
				<h1><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
				  <?php the_title(); ?>
				  </a>
				</h1>
				<div class="basic-blog-category post-meta-data"> 
					Posted By:<a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ));?>">
					<?php the_author(); ?>
					</a> 
					In<a href="#">
					  <?php   $cat_list = get_the_category_list();
					  if(!empty($cat_list)) { ?>
					  <?php the_category(', '); ?>
					</a>
					<?php } ?>
					on  <span><?php echo get_the_date( get_option( 'date_format' ) ); ?></span>
					
				</div>
				<p>
					<?php
						the_excerpt();
					?>
				</p>
					<?php wp_link_pages( array( 'before' => '<div class="link">' . __( 'Pages:', 'basic' ), 'after' => '</div>' ) ); ?>
			</article>
		</div>
	</div>